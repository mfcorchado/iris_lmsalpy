### Authors: Magnus M. Woods
### 21/09/2020: Original date
import warnings;
warnings.filterwarnings('ignore');

import os
import urllib
import tarfile
import warnings

import numpy as np
from copy import deepcopy

from astropy.wcs import WCS
from astropy import units as u
from astropy.coordinates import SkyCoord
import astropy.io.fits as fits

import iris_lmsalpy.hcr2fits as hcr2fits
import iris_lmsalpy.extract_irisL2data as extract_irisL2data

import sunpy.map
from sunpy.time import parse_time
from sunpy.net import Fido
from sunpy.net import attrs as a




def make_aia_pseudo_rast(sp, rst_wcs, aia_lst=[304], iris_line = 'Mg II k 2796', aia_dir='~/', aia_outdir = None, download = True, unzip=True, del_tar=True, del_fil=False, context_ims = False, verbose = True):
    '''This function produces AIA pseudorasters corresponding to the field of view of a give IRIS raster.

    It returns a dictionary containing the peudorasters in given AIA passbands along with the corresponding wcs object which defines coordinate system for the pseudo rasters.

    Parameters
    ----------

    sp: raster object,  This is the IRIS raster object created through using iris_lmsalpy.extract_irisL2data.raster
    
    rst_wcs: wcs object, This is the wcs object corresponding to the supplied spectra

    aia_lst: list of integers, this is a list of AIA passbands that the user wants to be converted to pseudo rasters. Default = [304]. 

    iris_line: string, the IRIS line that is being studied. Must be in the format present in the IRIS raster headers. Default = ''Mg II k 2796'. 

    aia_dir: string, This is the path to the location the user desires the compressed AIA files to be downloaded to. Default is the users home directory. 
    
    aia_outdir: string, This is the path to the directory where the AIA files will be uncompressed to. By default this is the same as aia_dir. 

    download: boolean, Download the AIA data cube corresponding to the supplied raster from the LMSAL servers. Default = True. 

    unzip: boolean,  This extracts the AIA fits files from the compressed file that is downloaded from the LMSAL servers. Default = True

    del_tar: boolean,  This deletes the compressed AIA data cube file. Default = True

    del_fil: boolean, This specifies whether the user wishes the downloaded AIA files to be deleted once the pseudo rater is created. Default = False. 
    
    context_ims: boolean,  This specifies whether or not larger field of view AIA images are saved for the time of each raster step. Warning, for large rasters, 
                            this can will take up a lot of memory. Default = False. 

    verbose:  boolean, Outputs information about the processes being run. Default = True

    Returns
    -------

    results: Dict, The function returns a dictionary, the contents of which depend on the value of context_im. 
                    If context_im is False, results is a dictionary containing the wcs object corresponding to the pseudo rasters, along with the individual pseudo rasters. Key names are the passbands. 
                    If conext_im is True, results is a dictionary containing two dictionaries.  One whose contents are as described above, and the second containing a wsc object and N-AIA images (where N is the number of raster steps) for each AIA passband the user requests
    
    Example
    --------
    First it is necessary to load the desired spectra and define the corresponding wcs object
    
    >>> sp = extract_irisL2data.load(raster_filename)
    >>> rst_wcs=WCS(extract_irisL2data.getheader(raster_filename,ext=6))
    
    Now we can run make_aia_pseudo_rast to download the corresponding AIA data cube. 
    Using the default parameters as we are here will download the AIA data cube, uncompress them in the download directory and then delete the compressed file. 
    
    >>> pseudo= make_aia_pseudo_rast(sp, rst_wcs, aia_lst=[304], iris_line = 'Mg II k 2796', aia_dir='path/to/aia/dir/', verbose = True)
    
    
    NOTE, the aia data cubes cover the whole observation that the specified raster 
    comes from, and as such have a very large file size. 
    '''

    # This recreates the IRISn file identifier for the data set
    iris_file = sp.raster['Mg II k 2796']['date_in_filename']+'_'+sp.raster['Mg II k 2796']['iris_obs_code']

    # Build the URL of the 
    aia_url = 'https://www.lmsal.com/solarsoft/irisa/data/level2_compressed/'+iris_file[0:4]+'/'+iris_file[4:6]+'/'+iris_file[6:8]+'/'+iris_file+'/iris_l2_'+iris_file+'_SDO.tar.gz'

    if download:
        # This downloads the file, but will take ages. 
        #### Need to add a progress bar for the download
        urllib.request.urlretrieve(aia_url, aia_dir+'iris_l2_'+iris_file+'_SDO.tar.gz')

    if not aia_outdir:
        aia_outdir = aia_dir

    if unzip:
        fname = aia_dir+'/iris_l2_'+iris_file+'_SDO.tar.gz'

        if fname.endswith("tar.gz"):
            if verbose:
                print('Unzipping tar.gz file')
            tar = tarfile.open(fname, "r:gz")
            tar.extractall(path = aia_outdir)
            tar.close()

            files = os.listdir(aia_dir)
            if verbose:
                print('Files that were extracted are:')
                for i in files:
                    if i.endswith('.fits'):
                        print(i)
                        print('')

        elif fname.endswith("tar"):
            print('Unzipping tar file')
            tar = tarfile.open(fname, "r:")
            tar.extractall(path = aia_outdir)
            tar.close()
            files = os.listdir(aia_dir)
            if verbose:
                print('Files that were extracted are:')
                for i in files:
                    if i.endswith('.fits'):
                        print(i)
                    print('')

        # Delete the tar file
        if del_tar:
            os.remove(fname)

    if not unzip:
        files = os.listdir(aia_dir)



    # Define the start and end times of the raster
    rast_st, rast_end = sp.raster[iris_line].date_time_acq_ok[0], sp.raster[iris_line].date_time_acq_ok[0-1]
    
    pseudo_rasters = {'wcs': (rst_wcs.dropaxis(0)).swapaxes(1, 0)}
    
    sji_maps = {}
    for aia in aia_lst:
        for p in files:
            if p.endswith(str(aia)+'.fits'):
                # Find the file for the passband we want
                k_file = aia_outdir+'/'+p
                # load in the file
                tmp = fits.open(k_file)
                # Define the AIA header
                hdr=tmp[0].header
                # Get the data
                hdr_dat = tmp[0].data
                # Update the header units so they can be parsed later
                hdr['CUNIT1'] = 'arcsec'
                hdr['CUNIT2'] = 'arcsec'
                hdr['CUNIT3'] = 's'
                # Close the fits file
                tmp.close()

                # Parse the AIA exposure start times
                parsed_aia = []
                aia_strtm = parse_time(hdr['date_obs'])
                # We loop over all the timesteps of the AIA data
                for i in range(hdr['NAXIS3']):
                    parsed_aia.append(aia_strtm+((12/(60*60*24))*i))

                # We make a list of whihc AIA image is closest to each raster step.
                aia_inds = []
                for i, rast_tim in enumerate(sp.raster[iris_line].date_time_acq_ok):
                    w = min(parsed_aia, key=lambda x: abs(x - parse_time(rast_tim)))
                    aia_inds.append(np.where(parsed_aia == w)[0][0])


                # Pull out the dimensions of the raster
                rast_dims = sp.raster[iris_line]['_raster__dim_data']
                # define adjustment parameters to find the half width/height of the raster
                x_px_adj = sp.raster[iris_line]['_raster__dim_data'][1]/2 - 0.5
                y_px_adj = sp.raster[iris_line]['_raster__dim_data'][0]/2 - 0.5
                px_adj = [x_px_adj, y_px_adj]

                # define the minimum X coordinate and the min/max y coordinates. In acrsec
                minx_coord = sp.raster[iris_line]['POS_X']-px_adj[0]*sp.raster[iris_line]['SPXSCL']
                #maxx_coord = sp.raster['Mg II k 2796']['POS_X']+px_adj[0]*sp.raster['Mg II k 2796']['SPXSCL']
                miny_coord = sp.raster[iris_line]['POS_Y']-px_adj[1]*sp.raster[iris_line]['SPYSCL']
                maxy_coord = sp.raster[iris_line]['POS_Y']+px_adj[1]*sp.raster[iris_line]['SPYSCL']


                # define the array where the sji FOV aia maps will be stored 
                sji_sub_maps = []
                # Now for each raster step
                for i in range(rast_dims[1]):
                    # Load the appropriate AIA image for this particular raster
                    aia_map = sunpy.map.GenericMap(hdr_dat[aia_inds[i], :, :], hdr)
                    #aia_map = sunpy.map.Map(downloaded_files[aia_inds[i]])
                    sub_maps.append(aia_map)
                    # Find the x, y coordinates of the given raster step
                    aia_xmin, aia_xmax = minx_coord+(i*sp.raster[iris_line]['SPXSCL']), minx_coord+((i+1)*sp.raster[iris_line]['SPXSCL'])
                    aia_ymin, aia_ymax = miny_coord, maxy_coord 

                    
                    # Raster FOV
                    # crop the full AIA image to the approximate IRIS FOV
                    top_right = SkyCoord(aia_xmax * u.arcsec, aia_ymax * u.arcsec, frame=aia_map.coordinate_frame)
                    bottom_left = SkyCoord(aia_xmin * u.arcsec, aia_ymin * u.arcsec, frame=aia_map.coordinate_frame)
                    aia_submap = aia_map.submap(bottom_left, top_right=top_right)


                    if context_ims:
                        # Full AIA FOV
                        # Currently using the FOV of the AIA data cube from LMSAL
                        # Crop so that it is the same Y FOV as the slit.
                        sji_top_right = SkyCoord(aia_map.top_right_coord.Tx.value * u.arcsec, aia_submap.top_right_coord.Ty.value* u.arcsec, frame=aia_map.coordinate_frame)
                        sji_bottom_left = SkyCoord(aia_map.bottom_left_coord.Tx.value * u.arcsec, aia_submap.bottom_left_coord.Ty.value * u.arcsec, frame=aia_map.coordinate_frame)
                        sji_sub_map = aia_map.submap(sji_bottom_left, top_right=sji_top_right)
                    
                        # Now rescale to IRIS Y pixel scale, and keep AIA aspect ration in X
                        if i == 0:
                            dims = [int((sji_sub_map.data.shape[1]/sji_sub_map.data.shape[0])*rast_dims[0]),rast_dims[0]]*u.pixel

                        sji_sub_map = sji_sub_map.resample(dims)
                    
                        # Define dictionary to fill with context images
                    
                        if i == 0:
                            # add the WCS for the given passband
                            sub_sjimaps = {'wcs': sji_sub_map.wcs}
                        
                        sji_sub_maps.append(sji_sub_map)
                    

                    # Now re-sample the AIA data  to have more x bins
                    if aia_submap.data.shape[1] >1:
                        new_dimensions = [40, aia_submap.dimensions.y.value] * u.pixel
                        aia_resampled_map = aia_submap.resample(new_dimensions)

                        # Now crop the image again to properly match the IRIS X FOV.
                        top_right = SkyCoord(aia_xmax * u.arcsec, aia_ymax * u.arcsec, frame=aia_resampled_map.coordinate_frame)
                        bottom_left = SkyCoord(aia_xmin * u.arcsec, aia_ymin * u.arcsec, frame=aia_resampled_map.coordinate_frame)
                        aia_subsubmap = aia_resampled_map.submap(bottom_left, top_right=top_right)
                        # Now resample to get the # of x pixels to 1 and the # of y pixels to be the IRIS #
                        new_dimensions = [1, rast_dims[0]] * u.pixel
                        aia_irisres_map = aia_subsubmap.resample(new_dimensions)
                    else:
                        aia_irisres_map = aia_submap

                    # Build the pesudo raster
                    if i == 0 :
                        pseudo_rst = aia_irisres_map.data
                        #aia_maps.append(aia_maps_iris)
                    else:
                        pseudo_rst = np.concatenate([pseudo_rst, aia_irisres_map.data], axis=1)
                        #aia_maps.append(aia_maps_iris)
                
                if context_ims:
                    sub_sjimaps.update({'data': sji_sub_maps})
        
        # Fill the pseudo raster dictionary
        pseudo_rasters.update({str(aia):pseudo_rst})
        
        if context_ims:
            # Fill the sji_maps dictionary
            sji_maps.update({str(aia): sub_sjimaps})
    if context_ims:
        results ={'pseudo_rasters': pseudo_rasters, 'context_maps':sji_maps}
    else:
        results ={'pseudo_rasters': pseudo_rasters}
    
    # Finally delete the file if desired                
    if del_fil:
        for file in files:
            os.remove(file)
            
    return results